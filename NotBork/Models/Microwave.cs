﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotBork.Models
{
    public class Microwave: Product
    {
        public int WavePower { get; set; }
        public double Volume { get; set; }
        public int ProgramsNumber { get; set; }
    }
}
