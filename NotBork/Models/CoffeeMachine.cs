﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotBork.Models
{
    public class CoffeeMachine : Product
    {
        public int Volume { get; set; }
        public int RecipesNumber { get; set; }
    }
}
