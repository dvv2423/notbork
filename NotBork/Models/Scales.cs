﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotBork.Models
{
    public class Scales : Product
    {
        public int MaxWeight { get; set; }
        public string Fabric { get; set; }
    }
}
