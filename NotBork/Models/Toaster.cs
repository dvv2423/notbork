﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotBork.Models
{
    public class Toaster : Product
    {
        public int ModesNumber { get; set; }
        public int FryingLevels { get; set; }

    }
}
