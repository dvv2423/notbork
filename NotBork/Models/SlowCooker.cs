﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotBork.Models
{
    public class SlowCooker : Product
    {
        public int Volume { get; set; }
        public int ModesNumber { get; set; }

    }
}
