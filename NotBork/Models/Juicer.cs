﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotBork.Models
{
    public class Juicer : Product
    {
        public double Volume { get; set; }
        public int SpeedsNumber { get; set; }
    }
}
