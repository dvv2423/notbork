﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotBork.Models
{
    public class Grill : Product
    {
        public int MaxTemp { get; set; }
        public int ModesNumber { get; set; }
    }
}
