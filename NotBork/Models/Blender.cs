﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotBork.Models
{
    public class Blender : Product
    {
        public int Id { get; set; }
        public double Volume { get; set; }
        public int ModesNumber { get; set; }
    }
}
