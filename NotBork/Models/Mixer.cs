﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotBork.Models
{
    public class Mixer : Product
    {
        public double Volume { get; set; }
        public int SpeersNumber { get; set; }
        public int ProgramsNumber { get; set; }
    }
}
